from flask import Flask
import os
from routes.routes import bp 

app = Flask(__name__)

PORT = os.environ['PORT_SCRAPING']

# Registra el blueprint en la aplicación
app.register_blueprint(bp)


if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True, port=PORT)
