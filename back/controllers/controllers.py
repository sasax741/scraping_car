from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def getProducts():
    # Configura las opciones de Chrome
    chrome_options = Options()
    
    # Configura el User-Agent para simular un navegador común
    chrome_options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36")
    
    chrome_options.add_argument('--disable-notifications')

    # Inicializa el controlador remoto de Selenium
    driver = webdriver.Remote(
        command_executor=f"http://selenium-chrome:4444/wd/hub",
        options=chrome_options
    )

    # URL de la página web que deseas abrir
    url = 'https://www.norauto.es/'

    # Abre la página web en el navegador
    driver.get(url)

    try:
        # Espera hasta que el elemento esté presente y sea clickeable
        boton = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="popin_tc_privacy_button_3"]'))
        )

        # Hacer clic en el botón
        boton.click()

    except Exception as e:
        print(f"No se pudo hacer clic en el botón: {e}")

    # Obtiene el HTML de la página
    pagina_html = driver.page_source

    # Imprime el HTML
    print(pagina_html)

    # Cierra el navegador
    driver.quit()

    return "Clic realizado con éxito después de cargar el elemento"

# Llama a la función para hacer clic en el botón
resultado = cosa()
print(resultado)
