from flask import Blueprint
from controllers.controllers import getProducts

bp = Blueprint('my_blueprint', __name__)

# Agrega una ruta al blueprint
@bp.route('/getProducts')
def getProductsRoute():
    return getProducts()